package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Time;
import java.util.Date;


/**
 * The persistent class for the programare database table.
 * 
 */
@Entity
@NamedQuery(name="Programare.findAll", query="SELECT p FROM Programare p")
public class Programare implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idprogramare;

	@Temporal(TemporalType.DATE)
	private Date date;

	private Time hour;

	private String name;

	private String reason;

	//bi-directional many-to-one association to Animal
	@ManyToOne
	@JoinColumn(name="fkAnimal")
	private Animal animal;

	//bi-directional many-to-one association to Personalmedical
	@ManyToOne
	@JoinColumn(name="idPersonal")
	private Personalmedical personalmedical;

	public Programare() {
	}

	public int getIdprogramare() {
		return this.idprogramare;
	}

	public void setIdprogramare(int idprogramare) {
		this.idprogramare = idprogramare;
	}

	public Date getDate() {
		return this.date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Time getHour() {
		return this.hour;
	}

	public void setHour(Time hour) {
		this.hour = hour;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getReason() {
		return this.reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Animal getAnimal() {
		return this.animal;
	}
	
	public String getAnimalName() {
		Animal anim = getAnimal();
		return anim.getName();
	}


	public void setAnimal(Animal animal) {
		this.animal = animal;
	}

	public Personalmedical getPersonalmedical() {
		return this.personalmedical;
	}
	
	public String getPersName() {
		Personalmedical pers = getPersonalmedical();
		return pers.getName();
	}

	public void setPersonalmedical(Personalmedical personalmedical) {
		this.personalmedical = personalmedical;
	}

}