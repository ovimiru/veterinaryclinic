package controllers;

import model.Animal;
import model.Personalmedical;
import model.Programare;
import util.DatabaseUtil;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

public class MainController implements Initializable{
	
	//-----------------Animal------------------------
	@FXML private TableView<Animal> animalTable;
	@FXML private TableColumn<Animal, Integer> idAnimal;
	@FXML private TableColumn<Animal, String> numeAnimal;
	@FXML private TextField fieldidanimal;
	@FXML private TextField fieldnumeanimal;
	//-----------------Employees------------------------
	@FXML private TableView<Personalmedical> angajatiTable;
	@FXML private TableColumn<Personalmedical, Integer> idAngajat;
	@FXML private TableColumn<Personalmedical, String> numeAngajat;
	@FXML private TableColumn<Personalmedical, String> pozitieAngajat;
	@FXML private TextField fieldidAngajat;
	@FXML private TextField fieldnumeAngajat;
	@FXML private TextField fieldpozitieAngajat;
	//-----------------Appointments------------------------
	@FXML private DatePicker dateAppointment;
	@FXML private ListView listAppointment;
	@FXML private Label nameAppointment;
	@FXML private Label angajatAppointment;
	@FXML private Label hourAppointment;
	@FXML private Label animalAppointment;
	@FXML private Label reasonAppointment;
	
	public ObservableList<Animal> anima;
	public ObservableList<Personalmedical> employees;
	public ObservableList<Programare> appointments;
	
	
	//Populate table with animals
	public void populateAnimalTable(){
		try {
		DatabaseUtil db = new DatabaseUtil();
		db.setup();
		List<Animal> animals = (List <Animal>) db.getAllAnimals();	
		anima = FXCollections.observableArrayList(animals);
		db.stop();
		}catch(Exception e) {
			System.out.println("Error");
		}
		
	}
	
	//Add an animal
	public void addAnimal() {
//		try {
//			DatabaseUtil db = new DatabaseUtil();
//			db.setUp();
//			int id = Integer.parseInt(fieldidanimal.getText());
//			String name = fieldnumeanimal.getText();
//			db.addAnimal(id,name);
//			db.closeConnection();
//			populateAnimalTable();
//			animalTable.setItems(anima);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
	}
	
	//Populate table for angajati
	public void populateAngajatiTable(){
//		try {
//		DatabaseUtil db = new DatabaseUtil();
//		db.setUp();
//		List<Personalmedical> employ = (List <Personalmedical>) db.getAllEmployees();	
//		employees = FXCollections.observableArrayList(employ);
//		db.closeConnection();
//		}catch(Exception e) {
//			System.out.println("Error");
//		}
		
	}
	
	//Add employee to database
	public void addEmployee() {
//		try {
//			DatabaseUtil db = new DatabaseUtil();
//			db.setUp();
//			int id = Integer.parseInt(fieldidAngajat.getText());
//			String name = fieldnumeAngajat.getText();
//			String pozitie = fieldpozitieAngajat.getText();
//			db.addEmployee(id,name,pozitie);
//			db.closeConnection();
//			populateAngajatiTable();
//			angajatiTable.setItems(employees);
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
	}
	
	public void populateAppointmentList() {
//		try {
//			DatabaseUtil db = new DatabaseUtil();
//			db.setUp();
//			List<Programare> appoint = (List <Programare>) db.getAllAppointments();	
//			appointments = FXCollections.observableArrayList(appoint);
//			db.closeConnection();
//		}catch(Exception e) {
//			System.out.println("Error");
//		}			
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1){
		// TODO Auto-generated method stub
//		populateAnimalTable();
//		populateAngajatiTable();
//		populateAppointmentList();
//		
//		idAnimal.setCellValueFactory(new PropertyValueFactory<Animal, Integer>("idAnimal"));
//		numeAnimal.setCellValueFactory(new PropertyValueFactory<Animal, String>("name"));
//		animalTable.setItems(anima);
//		
//		idAngajat.setCellValueFactory(new PropertyValueFactory<Personalmedical, Integer>("idPersonalMedical"));
//		numeAngajat.setCellValueFactory(new PropertyValueFactory<Personalmedical, String>("name"));
//		pozitieAngajat.setCellValueFactory(new PropertyValueFactory<Personalmedical, String>("position"));
//		angajatiTable.setItems(employees);
//		
//		listAppointment.setItems(appointments);
		
	}

}
