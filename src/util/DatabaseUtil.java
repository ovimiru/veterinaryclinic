package util;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.Doctor;

public class DatabaseUtil {

	private EntityManagerFactory entityManagerFctory;
	private EntityManager entityManeger;

	/**
	 * This is the setUp method
	 */
	public void setup() {
		entityManagerFctory = Persistence.createEntityManagerFactory("VeterinaryClininc");
		entityManeger = entityManagerFctory.createEntityManager();
	}

	/**
	 * this is the start transaction method
	 */
	public void startTransaction() {
		entityManeger.getTransaction().begin();
	}

	/**
	 * 
	 */
	public void commitTransaction() {
		entityManeger.getTransaction().commit();
	}

	/**
	 * 
	 */
	public void stop() {
		entityManeger.clear();
	}
	
	/**
	 * @param animal
	 */
	public void saveAnimal(Animal animal) {
		entityManeger.persist(animal);
	}
	
	/**
	 * @param appointment
	 */
	public void saveAppointment(Appointment appointment) {
		entityManeger.persist(appointment);
	}
	
	/**
	 * @param doctor
	 */
	public void saveDoctor(Doctor doctor) {
		entityManeger.persist(doctor);
	}
	
	/**
	 * @return all animals from db;
	 */
	public List<Animal> getAllAnimals() {
		List<Animal> animaList = (List<Animal>) entityManeger.createNativeQuery("SELECT * FROM petclinic.animal", Animal.class).getResultList();
		return animaList;
	}
	
	/**
	 * @return
	 */
	public List<Doctor> getAllDoctors() {
		List<Doctor> doctorList = (List<Doctor>) entityManeger.createNativeQuery("SELECT * FROM petclinic.doctor");
		return doctorList;
	}
	
	/**
	 * @return
	 */
	public List<Appointment> getAllAppointments() {
		List<Appointment> appointmentList = (List<Appointment>) entityManeger.createNativeQuery("SELECT * FROM petclinic.appointment");
		return appointmentList;
	}

}
